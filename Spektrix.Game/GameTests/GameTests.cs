﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Interfaces;
using Moq;
using Entities;
using Common;
using Business;

namespace GameTests
{
    [TestClass]
    public class GameTests
    {

        /// <summary>
        /// requirement:Rock beats scissors
        /// </summary>
        [TestMethod]
        public void TestRockWinsOverScissor()
        {
            Mock<IPlayer> HumanPlayer = new Mock<IPlayer>();
            Mock<GameRunner> gameRunner = new Mock<GameRunner>(PlayerTypes.random);
            Mock<IPlayer> player2 = new Mock<IPlayer>();
            player2.Setup(x => x.CurrentMove).Returns(Moves.Scissor);
            HumanPlayer.Setup(x => x.CurrentMove).Returns(Moves.Rock);
            gameRunner.Object.SetMaxRounds(3);
            gameRunner.Setup(x => x.player2).Returns(player2.Object);
            gameRunner.Setup(x => x.player1).Returns(HumanPlayer.Object);
            var response = gameRunner.Object.MakeNextMove((int)Moves.Rock);
            Assert.AreEqual(response.CurrentRoundResult, Result.win);
        }

        /// <summary>
        /// requirement:Scissors beats paper
        /// </summary>
        [TestMethod]
        public void TestScissorWinsOverPaper()
        {
            Mock<IPlayer> HumanPlayer = new Mock<IPlayer>();
            Mock<GameRunner> gameRunner = new Mock<GameRunner>(PlayerTypes.random);
            Mock<IPlayer> player2 = new Mock<IPlayer>();
            player2.Setup(x => x.CurrentMove).Returns(Moves.Paper);
            HumanPlayer.Setup(x => x.CurrentMove).Returns(Moves.Scissor);
            gameRunner.Object.SetMaxRounds(3);
            gameRunner.Setup(x => x.player2).Returns(player2.Object);
            gameRunner.Setup(x => x.player1).Returns(HumanPlayer.Object);
            var response = gameRunner.Object.MakeNextMove((int)Moves.Scissor);
            Assert.AreEqual(response.CurrentRoundResult, Result.win);
        }


        /// <summary>
        /// requirement:Paper beats rock
        /// </summary>
        [TestMethod]
        public void TestPaperWinsOverRock()
        {
            Mock<IPlayer> HumanPlayer = new Mock<IPlayer>();
            Mock<GameRunner> gameRunner = new Mock<GameRunner>(PlayerTypes.random);
            Mock<IPlayer> player2 = new Mock<IPlayer>();
            player2.Setup(x => x.CurrentMove).Returns(Moves.Rock);
            HumanPlayer.Setup(x => x.CurrentMove).Returns(Moves.Paper);
            gameRunner.Object.SetMaxRounds(3);
            gameRunner.Setup(x => x.player2).Returns(player2.Object);
            gameRunner.Setup(x => x.player1).Returns(HumanPlayer.Object);
            var response = gameRunner.Object.MakeNextMove((int)Moves.Paper);
            Assert.AreEqual(response.CurrentRoundResult, Result.win);
        }

        [TestMethod]
        public void TestGameConcludesAfterMaximumRounds()
        {
            Mock<IPlayer> HumanPlayer = new Mock<IPlayer>();
            HumanPlayer.Setup(x => x.CurrentMove).Returns(Moves.Paper);
            Mock<GameRunner> gameRunner = new Mock<GameRunner>(PlayerTypes.random);
            gameRunner.Setup(x => x.player2.CurrentMove).Returns(Moves.Rock);
            gameRunner.Setup(x => x.player1).Returns(HumanPlayer.Object);
            gameRunner.Object.SetMaxRounds(3);
            var responseround1 = gameRunner.Object.MakeNextMove((int)Moves.Paper);
            var responseround2 = gameRunner.Object.MakeNextMove((int)Moves.Paper);
            var responseround3 = gameRunner.Object.MakeNextMove((int)Moves.Paper);
            Assert.AreEqual(responseround3.state, GameState.Finished);
        }

        /// <summary>
        /// Requirement :The tactical computer player should always select the choice that would have beaten its last choice, e.g. if it played Scissors in game 2, it should play Rock in game 3.
        /// </summary>
        [TestMethod]
        public void TestTacticalPlayer()
        {
            Mock<TacticalComputerPlayer> TacticalPlayer = new Mock<TacticalComputerPlayer>();
            TacticalPlayer.Object.MakeNextMove();
            if (TacticalPlayer.Object.CurrentMove == Moves.Paper)
            {
                TacticalPlayer.Object.MakeNextMove();
                Assert.AreEqual(TacticalPlayer.Object.CurrentMove, Moves.Scissor);
            }
            else if (TacticalPlayer.Object.CurrentMove == Moves.Scissor)
            {
                TacticalPlayer.Object.MakeNextMove();
                Assert.AreEqual(TacticalPlayer.Object.CurrentMove, Moves.Rock);
            }
            else if (TacticalPlayer.Object.CurrentMove == Moves.Rock)
            {
                TacticalPlayer.Object.MakeNextMove();
                Assert.AreEqual(TacticalPlayer.Object.CurrentMove, Moves.Paper);
            }

        }

        /// <summary>
        /// Requriemnt :A match takes place between 2 players and is made up of 3 games, with the overall winner being the first player to win 2 games (i.e. best of 3).
        /// </summary>
        [TestMethod]
        public void TestGameEndsAtSecondRoundIfPlayerWinsTwice()
        {
            Mock<IPlayer> HumanPlayer = new Mock<IPlayer>();
            HumanPlayer.Setup(x => x.CurrentMove).Returns(Moves.Paper);
            Mock<GameRunner> gameRunner = new Mock<GameRunner>(PlayerTypes.random);
            HumanPlayer.Setup(x => x.points).Returns(2);
            gameRunner.Setup(x => x.player2.CurrentMove).Returns(Moves.Rock);
            gameRunner.Setup(x => x.player1).Returns(HumanPlayer.Object);
            gameRunner.Object.SetMaxRounds(3);
            var responseround2 = gameRunner.Object.MakeNextMove((int)Moves.Paper);
            Assert.AreEqual(responseround2.state, GameState.Finished);
        }

        [TestMethod]
        public void TestGameReturnsCorrectWinner()
        {
            Mock<IPlayer> player1 = new Mock<IPlayer>();
            Mock<IPlayer> player2 = new Mock<IPlayer>();
            player1.Setup(x => x.points).Returns(2);
            player2.Setup(x => x.points).Returns(1);
            Assert.AreEqual(Game.GetFinalResult(player1.Object, player2.Object), Result.win);
        }

        [TestMethod]
        public void TestGameReturnsCorrectStatusAfterloosingGame()
        {
            Mock<IPlayer> player1 = new Mock<IPlayer>();
            Mock<IPlayer> player2 = new Mock<IPlayer>();
            player1.Setup(x => x.points).Returns(1);
            player2.Setup(x => x.points).Returns(2);
            Assert.AreEqual(Game.GetFinalResult(player1.Object, player2.Object), Result.loose);
        }
    }
}

