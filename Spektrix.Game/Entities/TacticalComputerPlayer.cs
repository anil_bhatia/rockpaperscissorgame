﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace Entities
{
    public class TacticalComputerPlayer : BasePlayer, IPlayer
    {
        private bool isFirstMove { get; set; }     
       
        public override void MakeNextMove()
        {
            if (isFirstMove == true)
            {
                isFirstMove = false;
                CurrentMove = (Moves)new Random().Next(1, 4);
            }
            else
            {
                if (CurrentMove == Moves.Paper)
                    CurrentMove = Moves.Scissor;
                else if (CurrentMove == Moves.Rock)
                    CurrentMove= Moves.Paper;
                else CurrentMove = Moves.Rock;
            }
        }
    }
}
