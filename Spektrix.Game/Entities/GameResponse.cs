﻿using Common;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class GameResponse
    {
        public GameState state;
        public IPlayer Winner { get; set; }
        public Result CurrentRoundResult {get;set;}
        public Result FinalResult { get; set; }
    }
}
