﻿using Common;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class BasePlayer
    {
        public int points { get; set; }
        public string Name { get; set; }
        public Moves CurrentMove { get; set; }
        public virtual void MakeNextMove() { }
       

    }
}
