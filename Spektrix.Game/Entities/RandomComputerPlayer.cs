﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace Entities
{       
    public class RandomComputerPlayer : BasePlayer, IPlayer
    {
        public override void MakeNextMove()
        {
            Random rnd = new Random();
            var randomValue =rnd.Next(1, 3);
            CurrentMove =(Moves)randomValue;                        
        }
    }
}
