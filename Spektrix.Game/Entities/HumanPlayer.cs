﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace Entities
{
    public class HumanPlayer : BasePlayer, IPlayer
    {       
        public void SetValue(Moves currentMove)
        {
            this.CurrentMove = currentMove;
        }
    }
}
