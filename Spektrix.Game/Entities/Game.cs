﻿using Common;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public static  class Game
    {
        public  static Result GetResult(Moves move1, Moves move2)
        {
            if (move1 == move2)
                return Result.draw;
            else if ((move1 == Moves.Rock) && (move2 == Moves.Scissor) ||
                (move1 == Moves.Paper) && (move2 == Moves.Rock) ||
                (move1 == Moves.Scissor) && (move2 == Moves.Paper))
                return Result.win;
            else
                return Result.loose;
        }

        public static Result GetFinalResult(IPlayer humanPlayer,IPlayer player)
        {
            if (humanPlayer.points > player.points)
                return Result.win;
            else if (humanPlayer.points < player.points)
                return Result.loose;
            else
                return Result.draw;
            
        }

    }
}
