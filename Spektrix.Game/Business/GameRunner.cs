﻿using Common;
using Entities;
using Interfaces;

namespace Business
{
    public class GameRunner
    {

        public virtual IPlayer player1 { get; set; }
        public virtual IPlayer player2 { get; set; }
        private int MaxRounds;
        private int roundsCounter;
        private int drawcounters;

        //Dependency injection can be used here by creating factory class but things are kept simple at this point.
        public GameRunner(PlayerTypes playerType2)
        {
            if (playerType2 == PlayerTypes.random)
                player2 = new RandomComputerPlayer();
            else
                player2 = new TacticalComputerPlayer();

            player1 = new HumanPlayer();
        }

        public Moves getCurrentMove()
        {
            return player2.CurrentMove;
        }

        /// <summary>
        /// Dynamically sets the rounds required by the calling application
        /// </summary>
        /// <param name="maxRounds"></param>
        public void SetMaxRounds(int maxRounds)
        {
            MaxRounds = maxRounds;
        }

        /// <summary>
        /// Makes next move and returns the result. If all the rounds are completed, returns the final result information in response
        /// </summary>
        /// <param name="selectedvalue"></param>
        /// <returns></returns>
        public GameResponse MakeNextMove(int selectedvalue)
        {           
            if (roundsCounter < MaxRounds)
            {
                player1.CurrentMove = (Moves)selectedvalue;
                player2.MakeNextMove();

                var result = Game.GetResult(player1.CurrentMove, player2.CurrentMove);
                if (result == Common.Result.win)
                    player1.points++;
                else if (result == Result.loose)
                    player2.points++;
                else
                    drawcounters++;
                roundsCounter++;
                int DecidingRounds = MaxRounds - drawcounters;
                return roundsCounter == MaxRounds || player1.points > DecidingRounds / 2 || player2.points > DecidingRounds / 2 ? new GameResponse { state = Common.GameState.Finished, FinalResult = Game.GetFinalResult(player1, player2), CurrentRoundResult=result } : new GameResponse { state = Common.GameState.InProgress, Winner = null, CurrentRoundResult = result };
            }
            else
                throw new System.Exception("All Rounds Completed");
        }

            

    }
}
