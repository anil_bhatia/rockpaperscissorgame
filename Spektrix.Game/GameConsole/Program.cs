﻿using Business;
using Common;
using Entities;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameConsole
{
    class Program
    {
        static void Main(string[] args)
        {   
            //Get the Name of the Human player
            Console.WriteLine("Welcome to Game!!!.Please enter your name:");
            var name = Console.ReadLine();

            //Get the second player type option from the user
            Console.WriteLine("Please Choose your opponent:  1 for Random Computer,2 for Tactical Computer ");
            var opponent = Console.ReadLine();

            //keep prompting the user until right input is entered
            while (opponent != "1" && opponent != "2")
            {
                Console.WriteLine("Invalid Opponent : Please Choose your opponent:  1 for Random Computer,2 for Tactical Computer ");
                opponent = Console.ReadLine();
            }

            //Initialize the game with the selected second playertype. dependency injection can be used here but things are kept simple at this point.
            GameRunner game;
            if (opponent == "1")
            {
                 game = new GameRunner(PlayerTypes.random); 
            }
            else
            {
                 game  = new GameRunner(PlayerTypes.human);
            }
             
            //set the max rounds. As per the requirement the can be changed to set the maximum number of rounds
            game.SetMaxRounds(3);        
            GameResponse gameResponse = new GameResponse();
            Console.WriteLine("Starting Game:");

            //Get the option Rock,Paper or Scissor from the user
            while (gameResponse.state != GameState.Finished)
            {
                Console.WriteLine("Please Enter your choice:1 for Rock,2 for Paper and 3 for Scissor");
                var selectedValue = Console.ReadLine();

                //keep prompting the user until right input is entered
                while (selectedValue != "1" && selectedValue != "2" && selectedValue != "3")
                {
                    Console.WriteLine("Invalid Option : Please Choose correct option:  1 for Random Computer,2 for Tactical Computer ");
                    selectedValue = Console.ReadLine();
                }
                gameResponse = game.MakeNextMove(int.Parse(selectedValue));
                var currentMove = game.getCurrentMove();
                if (gameResponse.CurrentRoundResult == Result.win)
                {
                    Console.WriteLine("Congratulations you won !!! The Computer Selected {0}", currentMove);
                }
                else if (gameResponse.CurrentRoundResult == Result.loose)
                {
                    Console.WriteLine("Sorry you lost !!! The Computer Selected {0}",currentMove);
                }
                else
                {
                    Console.WriteLine("Its a draw  !!! The Computer Selected {0}", currentMove);
                }
            }
            
            Console.WriteLine("Final Result :");

            if (gameResponse.FinalResult == Result.win)
            {
                Console.WriteLine("Congratulations {0} you won the match",name);
            }
            else if (gameResponse.FinalResult == Result.loose)
            {
                Console.WriteLine("Sorry {0} you lost the match!! Better luck next time",name);
            }
            else
            {
                Console.WriteLine("Its a draw {0}!! Better luck next time", name);
            }
            Console.WriteLine("Enter any key to exit.");
            Console.ReadLine();
        }
    }
}
