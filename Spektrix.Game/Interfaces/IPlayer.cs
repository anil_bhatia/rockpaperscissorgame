﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IPlayer
    {
        string Name { get; set; }
        int points { get; set; }
        void MakeNextMove();
        Moves CurrentMove { get; set; }
    }
}
